<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Gym Equipment | Gloversure</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;700&display=swap" />
    <link rel="stylesheet" href="css/main.css" />
</head>
<body>
    <div class="hero">
        <?php for($i = 0; $i < 2; $i++): ?>
        <div class="item">
            <div class="image">
                <picture>
                    <source media="(max-width:767px)" srcset="https://via.placeholder.com/1000x600">
                    <img src="https://via.placeholder.com/1920x600" alt="Hand Made Gym Equipment" />
                </picture>
            </div>
            <div class="content">
                <h1>Hand Made <span>Gym Equipment</span></h1>
                <a href="#" title="Shop Now" class="button large">
                    <span>Shop Now</span>
                </a>
            </div>
        </div>
        <?php endfor; ?>
    </div>

    <div class="heading heading-categories">
        <div class="heading-wrapper">
            <h2>
                <span class="hollow">Strength</span>
                <span class="primary">Our Products</span>
                <span class="standard">What Do We Offer.</span>
            </h2>
        </div>
        <div class="button-wrapper">
            <a href="#" title="View All Categories" class="button large">
                <span>View All Categories</span>
            </a>
        </div>
    </div>

    <div class="categories grid-container">
        <?php for($i = 0; $i < 6; $i++): ?>
        <div class="item">
            <img src="https://via.placeholder.com/500x400" alt="Weight Discs" />
            <h3>Weight Discs</h3>
        </div>
        <?php endfor; ?>
    </div>

    <div class="market-leaders">
        <div class="grid-container">
            <picture>
                <source media="(max-width:1023px)" srcset="https://via.placeholder.com/1000x600">
                <img src="https://via.placeholder.com/400x700" alt="So Why Use JMC Strength" />
            </picture>

            <div class="content">
                <div class="heading">
                    <div class="heading-wrapper">
                        <h2>
                            <span class="hollow">Strong</span>
                            <span class="primary">Market Leaders</span>
                            <span class="standard">So Why Use JMC Strength</span>
                        </h2>
                    </div>
                </div>

                <div class="inner-text">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum quis tortor id risus convallis porta vel vel orci. Cras id diam nec tortor finibus cursus sed eget nunc. Duis nunc ligula, imperdiet et neque sed, accumsan consequat ex.</p>

                    <ul>
                        <li>
                            Hand crafted gym equipment
                        </li>
                        <li>
                            Quality British made
                        </li>
                        <li>
                            Another awesome point
                        </li>
                        <li>
                            Another awesome point
                        </li>
                        <li>
                            Another awesome point
                        </li>
                    </ul>
                </div>

                <div class="button-wrapper">
                    <a href="#" title="Shop Now" class="button large">
                        <span>Shop Now</span>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="top-products">
        <div class="grid-container">
            <div class="heading">
                <div class="heading-wrapper">
                    <h2>
                        <span class="hollow">Products</span>
                        <span class="primary">Top Products</span>
                        <span class="standard">Shop Now</span>
                    </h2>
                </div>
            </div>

            <div class="products">
                <?php for($i = 0; $i < 6; $i++): ?>
                <div class="item">
                    <img src="https://via.placeholder.com/500x500" alt="Product" />
                    <h4>Product Title Here</h4>

                    <div class="price-wrapper">
                        <p>Prices from</p>
                        <p class="price">£299</p>
                    </div>

                    <a href="#" class="button large">
                        <span>Shop Now</span>
                    </a>
                </div>
                <?php endfor; ?>
            </div>
        </div>
    </div>

    <script src="js/vendor/jquery.min.js"></script>
    <script src="js/vendor/slick.min.js"></script>
    <script src="js/main.js"></script>
</body>
</html>
